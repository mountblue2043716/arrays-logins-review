const data = require('./2-arrays-logins.cjs');
let idsToCount = ['.com', '.org', '.au'];
let output = data.reduce((acc, item) => {
    idsToCount.forEach((id) => {
        if (item.email.endsWith(id)) {
            acc[id] = (acc[id] || 0) + 1;
        }
    })
    return acc;
}, {});
console.log(output);